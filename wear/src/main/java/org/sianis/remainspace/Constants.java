package org.sianis.remainspace;

public class Constants {
    public static final String FREE_SPACE_REQUEST_PATH = "/request";
    public static final String FREE_SPACE_RESPONSE_PATH = "/response";
    public static final String FREE_SPACE_RESPONSE_ACTION = "FREE_SPACE_RESPONSE_ACTION";
    public static final String FREE_SPACE = "FREE_SPACE";
}
