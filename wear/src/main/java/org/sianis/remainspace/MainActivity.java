package org.sianis.remainspace;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.wearable.view.CardFragment;
import android.support.wearable.view.WatchViewStub;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import java.nio.ByteBuffer;

public class MainActivity extends Activity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, MessageApi.MessageListener {

    private static final String TAG = "MainActivity.Wear";

    //Views on activity
    private TextView mTextView;
    private Button mButton;

    //Communication part
    private GoogleApiClient mGoogleApiClient;
    private Node mNode;
    private boolean mResolvingError = false;
    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //We use WatchViewStub so need to wait for views
        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                mTextView = (TextView) stub.findViewById(R.id.text);
                mButton = (Button) stub.findViewById(R.id.button);
                mButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        queryFreeSpace();
                    }
                });
            }
        });

        //Connect the GoogleApiClient
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    @Override
    public void onConnected(Bundle bundle) {
        //Client connected, find handheld node
        resolveNode();
        //Add data listener
        Wearable.MessageApi.addListener(mGoogleApiClient, this);
    }

    private void resolveNode() {
        Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).setResultCallback(new ResultCallback<NodeApi.GetConnectedNodesResult>() {
            @Override
            public void onResult(NodeApi.GetConnectedNodesResult nodes) {
                for (Node node : nodes.getNodes()) {
                    //First node is enough
                    mNode = node;
                    break;
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        //Connect if client is disconnected
        if (!mResolvingError && !mGoogleApiClient.isConnected() && !mGoogleApiClient.isConnecting()) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onStop() {
        if (null != mGoogleApiClient && mGoogleApiClient.isConnected()) {
            //Disconnect data listener
            Wearable.MessageApi.removeListener(mGoogleApiClient, this);
            //Disconnect client
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    @Override
    public void onConnectionSuspended(int i) {
        //Not important now
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        //Not important now
    }

    private void queryFreeSpace() {
        //Send message to handheld iff client is connected
        if (mNode != null && mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            Wearable.MessageApi.sendMessage(
                    mGoogleApiClient, mNode.getId(), Constants.FREE_SPACE_REQUEST_PATH, null).setResultCallback(

                    new ResultCallback<MessageApi.SendMessageResult>() {
                        @Override
                        public void onResult(MessageApi.SendMessageResult sendMessageResult) {
                            if (!sendMessageResult.getStatus().isSuccess()) {
                                //Write log if send is failed due to error
                                Log.e(TAG, "Failed to send message with status code: " + sendMessageResult.getStatus().getStatusCode());
                            }
                        }
                    }
            );
        } else {
            //Improve your code
        }
    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        /*
         * Receive the message from mobile
         */
        if (Constants.FREE_SPACE_RESPONSE_PATH.equals(messageEvent.getPath())) {
            //Message contains long in data byte[]
            ByteBuffer buffer = ByteBuffer.wrap(messageEvent.getData());
            long free = buffer.getLong();
            final String freeText;
            //Handle GB and MB
            if (free > 1024) {
                freeText = getString(R.string.free_space_gb, free / 1024f);
            } else {
                freeText = getString(R.string.free_space_mb, free);
            }
            //Show message
            handler.post(new Runnable() {
                @Override
                public void run() {
                    mButton.setVisibility(View.GONE);
                    CardFragment cardFragment = CardFragment.create(getString(R.string.free_space_title), freeText, R.drawable.ic_launcher);
                    getFragmentManager().beginTransaction().add(R.id.frame_layout, cardFragment).commit();
                }
            });
        }

    }
}
