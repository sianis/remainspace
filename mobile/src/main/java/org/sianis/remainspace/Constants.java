package org.sianis.remainspace;

public class Constants {
    public static final String FREE_SPACE_REQUEST_PATH = "/request";
    public static final String FREE_SPACE_RESPONSE_PATH = "/response";
}
