package org.sianis.remainspace;

import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;

import java.nio.ByteBuffer;

public class ListenerServiceFromWear extends WearableListenerService implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = "ListenerServiceFromWear.Mobile";

    //Communication part
    private GoogleApiClient mGoogleApiClient;
    private Node mNode;
    private long answer = -1;

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        super.onMessageReceived(messageEvent);

        //Every message create a client instance
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Wearable.API)
                .build();

        //Generate answer
        if (Constants.FREE_SPACE_REQUEST_PATH.equals(messageEvent.getPath())) {
            Log.d(TAG, "FREE_SPACE_REQUEST_PATH received");
            StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
            long bytesAvailable = stat.getBlockSizeLong() * stat.getBlockCountLong();
            answer = bytesAvailable / 1048576;
        }

        //Connect when answer is ready
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        //Client connected, find wearable node
        resolveNode();
    }

    private void resolveNode() {
        Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).setResultCallback(new ResultCallback<NodeApi.GetConnectedNodesResult>() {
            @Override
            public void onResult(NodeApi.GetConnectedNodesResult nodes) {
                for (Node node : nodes.getNodes()) {
                    //First node is enough
                    mNode = node;
                }
                sendPendingMessage();
            }
        });
    }

    private void sendPendingMessage() {
        //Send message to wearable
        if (answer != -1 && mNode != null && mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            byte[] bytes = ByteBuffer.allocate(8).putLong(answer).array();
            Wearable.MessageApi.sendMessage(
                    mGoogleApiClient, mNode.getId(), Constants.FREE_SPACE_RESPONSE_PATH, bytes).setResultCallback(

                    new ResultCallback<MessageApi.SendMessageResult>() {
                        @Override
                        public void onResult(MessageApi.SendMessageResult sendMessageResult) {
                            if (!sendMessageResult.getStatus().isSuccess()) {
                                Log.e(TAG, "Failed to send message with status code: " + sendMessageResult.getStatus().getStatusCode());
                            } else {
                                Log.d(TAG, "FREE_SPACE_RESPONSE_PATH sent");
                            }
                            answer = -1;
                        }
                    }
            );
        } else {
            //Improve your code
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        //Not important now
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        //Not important now
    }
}
